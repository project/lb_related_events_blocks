#  Layout Builder Related Events Blocks

Layout Builder Related Events Blocks is a Helper module for the custom "Related Events" block type.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/lb_related_events_blocks).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/lb_related_events_blocks).


## Table of contents

- Requirements
- Installation
- Configuration
- Styles
- Maintainers


## Requirements

No special requirements.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

The module has no menu or modifiable settings. There is no configuration. When
enabled, the module will prevent the links from appearing. To get the links
back, disable the module and clear caches.


## Styles

1. Run `npm i`
2. `npm run build` to build styles
3. `npm run watch` to watch and rebuild styles


## Maintainers

- Max - [mkdok](https://www.drupal.org/u/casey)
- Anatolii Poliakov - [anpolimus](https://www.drupal.org/u/anpolimus)
- Dima Danylevskyi - [danylevskyi](https://www.drupal.org/u/danylevskyi)
- Avi Schwab - [froboy](https://www.drupal.org/u/froboy)
- Andrii Podanenko - [podarok](https://www.drupal.org/u/podarok)
- Andrey Zbukar - [andrey_zb](https://www.drupal.org/u/andrey_zb)

